<?php

// Load the release files (concatenated, compressed)
define("LOAD_RELEASE", 1);
// Load the debug files (concatenated, not compressed)
define("LOAD_DEBUG", 2);
// Load the original source files (for developers)
define("LOAD_SOURCE", 3);

// Disable PHP5>PHP4 compatibility, this could segfault apache with xml_parser_free
ini_set('zend.ze1_compatibility_mode', false);         
?>
