<?php
	/**
	* Download Attachment
	* This file is used to download an attachment of a message.
	* @FIXME this file has been changed in trunk significantly so update it from trunk
	*/
	function downloadAttachment() {
		// Get store id
		$storeid = false;
		if(isset($_GET["store"])) {
			$storeid = $_GET["store"];
		}

		// Get message entryid
		$entryid = false;
		if(isset($_GET["entryid"])) {
			$entryid = $_GET["entryid"];
		}

		// Check which type isset
		$openType = "inline";
		if(isset($_GET["openType"])) {
			$openType = $_GET["openType"];
		}

		// Get number of attachment which should be opened.
		$attachNum = false;
		if(isset($_GET["attachNum"])) {
			$attachNum = $_GET["attachNum"];
		}

		// Check if inline image should be opened
		$attachCid = false;
		if(isset($_GET["attachCid"])) {
			$attachCid = $_GET["attachCid"];
		}

		// Check if storeid and entryid isset
		if($storeid && $entryid) {
			// Open the store
			$store = $GLOBALS["mapisession"]->openMessageStore(hex2bin($storeid));
			
			if($store) {
				// Open the message
				$message = mapi_msgstore_openentry($store, hex2bin($entryid));
				
				if($message) {
					$attachment = false;

					// Check if attachNum isset
					if($attachNum) {
						// Loop through the attachNums, message in message in message ...
						for($i = 0; $i < (count($attachNum) - 1); $i++)
						{
							// Open the attachment
							$tempattach = mapi_message_openattach($message, (int) $attachNum[$i]);
							if($tempattach) {
								// Open the object in the attachment
								$message = mapi_attach_openobj($tempattach);
							}
						}

						// Open the attachment
						$attachment = mapi_message_openattach($message, (int) $attachNum[(count($attachNum) - 1)]);
					} else if($attachCid) { // Check if attachCid isset
						// Get the attachment table
						$attachments = mapi_message_getattachmenttable($message);
						$attachTable = mapi_table_queryallrows($attachments, Array(PR_ATTACH_NUM, PR_ATTACH_CONTENT_ID));

						foreach($attachTable as $attachRow)
						{
							// Check if the CONTENT_ID is equal to attachCid
							if(isset($attachRow[PR_ATTACH_CONTENT_ID]) && $attachRow[PR_ATTACH_CONTENT_ID] == $attachCid) {
								// Open the attachment
								$attachment = mapi_message_openattach($message, $attachRow[PR_ATTACH_NUM]);
							}
						}
					}

					// Check if the attachment is opened
					if($attachment) {
						// Get the props of the attachment
						$props = mapi_attach_getprops($attachment, array(PR_ATTACH_LONG_FILENAME, PR_ATTACH_MIME_TAG, PR_DISPLAY_NAME, PR_ATTACH_METHOD));
						// Content Type
						$contentType = "application/octet-stream";
						// Filename
						$filename = "ERROR";

						// Set filename
						if(isset($props[PR_ATTACH_LONG_FILENAME])) {
							$filename = $props[PR_ATTACH_LONG_FILENAME];
						} else if(isset($props[PR_ATTACH_FILENAME])) {
							$filename = $props[PR_ATTACH_FILENAME];
						} else if(isset($props[PR_DISPLAY_NAME])) {
							$filename = $props[PR_DISPLAY_NAME];
						} 
				
						// Set content type
						if(isset($props[PR_ATTACH_MIME_TAG])) {
							$contentType = $props[PR_ATTACH_MIME_TAG];
						} else {
							// Parse the extension of the filename to get the content type
							if(strrpos($filename, ".") !== false) {
								$extension = strtolower(substr($filename, strrpos($filename, ".")));
								$contentType = "application/octet-stream";
								if (is_readable("mimetypes.dat")){
									$fh = fopen("mimetypes.dat","r");
									$ext_found = false;
									while (!feof($fh) && !$ext_found){
										$line = fgets($fh);
										preg_match("/(\.[a-z0-9]+)[ \t]+([^ \t\n\r]*)/i", $line, $result);
										if ($extension == $result[1]){
											$ext_found = true;
											$contentType = $result[2];
										}
									}
									fclose($fh);
								}
							}
						}
						
						// Set the headers
						header("Pragma: public");
						header("Expires: 0"); // set expiration time
						header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
						header("Content-Disposition: " . $openType . "; filename=\"" . browserDependingHTTPHeaderEncode($filename) . "\"");
						header("Content-Transfer-Encoding: binary");
						
						// Set content type header
						if($openType == "attachment") {
							header("Content-Type: application/octet-stream");
						} else {
							header("Content-Type: " . $contentType);
						}

						// Open a stream to get the attachment data
						$stream = mapi_openpropertytostream($attachment, PR_ATTACH_DATA_BIN);
						$stat = mapi_stream_stat($stream);
						// File length
						header("Content-Length: " . $stat["cb"]);
						for($i = 0; $i < $stat["cb"]; $i += BLOCK_SIZE) {
							// Print stream
							echo mapi_stream_read($stream, BLOCK_SIZE);
						}
					}
				}
			}
		} else { // Open a uploaded attachment
			if (isset($_GET["dialog_attachments"])) {
				// return recent uploaded file 
				$attachment_state = new AttachmentState();
				$attachment_state->open();

				$tmpname = $attachment_state->getAttachmentPath($attachNum[0]);
				$fileinfo = $attachment_state->getAttachmentFile($_GET["dialog_attachments"], $attachNum[0]);

				// Check if the file still exists
				if (is_file($tmpname)) {
					// Set the headers
					header("Pragma: public");
					header("Expires: 0"); // set expiration time
					header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
					header("Content-Disposition: " . $openType . "; filename=\"" . browserDependingHTTPHeaderEncode($fileinfo['name']) . "\"");
					header("Content-Transfer-Encoding: binary");
					header("Content-Type: application/octet-stream");
					header("Content-Length: " . filesize($tmpname));

					// Open the uploaded file and print it
					$file = fopen($tmpname, "r");
					fpassthru($file);
					fclose($file);
				}
			}
		}
	}

	// directly execute the function because this function is only created to not clutter global namespace
	downloadAttachment();
?>
