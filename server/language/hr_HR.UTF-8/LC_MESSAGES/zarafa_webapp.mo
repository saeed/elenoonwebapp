��    !      $  /   ,      �     �     �     �     �  $        +  
   4     ?     F     N     S  	   X     b     h     l  	   s     }     �     �     �     �     �     �     �  	   �     �  +   �               #  	   +     5  �  D     &     .     6     ?     H     g     p     ~  	   �     �     �     �     �     �     �     �     �     �     �               !     4     =     D     J  .   S  	   �     �     �     �     �                                                                      
       !      	                                                                            April August Cancel December Displaying messages {0} - {1} of {2} February First Page Friday January July June Last Page March May Monday Next Page No messages to display November OK October Page {A} of {0} Previous Page Refresh Saturday September Sunday The quick brown fox jumps over the lazy dog Thursday Today Tuesday Wednesday {0} (Spacebar) Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-02-16 00:02+0100
PO-Revision-Date: 2012-08-09 07:35+0200
Last-Translator: Tomislav <tomislav.malnar@tzv-gredelj.hr>
Language-Team: LANGUAGE <LL@li.org>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Pootle 2.1.5
 Travanj Kolovoz Odustani Prosinac Prikaz poruka {0} - {1} od {2} Veljača Prva stranica Petak Siječanj Srpanj Lipanj Posljednja stranica Ožujak Svibanj Ponedjeljak Sljedeća stranica Nema poruka za prikaz Studeni U redu Listopad Stranica {A} od {0} Prethodna stranica Osvježi Subota Rujan Nedjelja Brza smeđa lisica preskače preko lijenog psa Četvrtak Danas Utorak Srijeda {0} (Razmaknica) 