��    ?        Y         p     q     �     �     �     �     �     �     �     �     �       1     &   O  .   v     �     �     �  $   �     �  
   �  
          	        $     <     L     T     Y  	   ^     h     �     �     �     �     �  	   �     �     �     �                 !   #     E     c     q     y  	   �     �     �     �  +   �  #   �  $   	     6	     ?	     E	     M	     `	  	   }	     �	     �	  ~  �	          5  !   S  %   u  
   �  
   �     �     �     �     �  &   �  =     -   ]  J   �     �     �       .     
   @     K     _     m     v  !   �     �     �  
   �     �     �  .   �  4   $     Y     b     g     t     �  2   �     �     �  
     
          7   /     g     �     �     �     �  '   �  *   �       +     E   I  A   �     �  
   �     �     �  7        M     ^     w               )   2      ?       (      &   .   ;   0          1           -       3         "            5      :                     /   $                              	             +   4           *                     9   %   ,   '       !   8           #      7                 =          6       
          <          >    Align Text Left Align Text Right Align text to the left. Align text to the right. April August Bold (Ctrl+B) text Bullet List Cancel Center Text Center text in the editor. Change the background color of the selected text. Change the color of the selected text. Choose a month (Control+Up/Down to move years) December Decrease the font size. Disabled Displaying messages {0} - {1} of {2} February First Page Font Color Friday Hyperlink Increase the font size. Italic (Ctrl+I) January July June Last Page Make the selected text bold. Make the selected text italic. March May Monday Next Month (Control+Right) Next Page No messages to display November Numbered List OK October Page {A} of {0} Please enter the URL for the link Previous Month (Control+Left) Previous Page Refresh Saturday September Start a bulleted list. Start a numbered list. Sunday The quick brown fox jumps over the lazy dog This date is after the maximum date This date is before the minimum date Thursday Today Tuesday Underline (Ctrl+U) Underline the selected text. Wednesday d/m/Y {0} (Spacebar) Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-02-16 00:02+0100
PO-Revision-Date: 2012-12-14 14:00+0200
Last-Translator: Amir <xfshxr@yahoo.com>
Language-Team: LANGUAGE <LL@li.org>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.1.5
 تزاز متن از چپ تزار متن از راست تراز متن از سمت چپ. تراز متن از سمت راست. آوریل آگوست متن ضخیم (Ctrl+B) لیست گردک لغو متن در مرکز متن در مرکز ویرایشگر. تغییر رنگ پس زمینه متن انتخاب شده. تغییر رنگ متن انتخاب شده. انتخاب یک ماه (Control+Up/Down برای تغییر سال ها) دسامبر کاهش اندازه قلم. غیر فعال نمایش پیام های {0} - {1} از  {2} فوریه اولین صفحه رنگ قلم جمعه پیوند دار افزایش اندازه قلم. ایتالیک (Ctrl+I) ژانویه جولای جون آخرین صفحه متن انتخاب شده را ضخیم کن. متن انتخاب شده را ایتالیک کن. مارس می دوشنبه ماه بعدی (Control+Right) صفحه بعدی پیامی برای نمایش موجود نیست نوامبر فهرست شماره دار تایید اکتبر صفحه {A} از {0} لطفا پیوند را برای URL وارد کنید ماه قبلی (Control+Left) صفحه قبلی نوسازی شنبه سپتامبر آغاز یک لیست گردک شده. آغاز یک فهرست شماره دار یکشنبه The quick brown fox jumps over the lazy dog این تاریخ بعد از بیشترین تاریخ می باشد این تاریخ قبل از حداقل تاریخ می باشد پنجشنبه امروز سه شنبه زیر خط دار (Ctrl+U) متن انتخاب شده را زیرخط دار کن. چهارشنبه روز / ماه / سال {0} (Spacebar) 