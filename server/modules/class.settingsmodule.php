<?php
	/**
	* Settings Module
	*/
	class SettingsModule extends Module
	{
		/**
		* Constructor
		* @param int $id unique id.
		* @param array $data list of all actions.
		*/
		function SettingsModule($id, $data)
		{
			parent::Module($id, $data);
		}
		
		/**
		* Executes all the actions in the $data variable.
		* @return boolean true on success or false on fialure.
		*/
		function execute()
		{
			foreach($this->data as $actionType => $action)
			{
				if(isset($actionType)) {
					try {
						switch ($actionType) {
							case "retrieveAll":
								$this->retrieveAll($actionType);
								break;
							case "set":
								$this->set($action["setting"]);
								break;
							case "delete":
								$this->deleteSetting($action["setting"]);
								break;
							default:
								$this->handleUnknownActionType($actionType);
						}
					} catch (SettingsException $e) {
						$this->sendFeedback(false, $this->errorDetailsFromException($e));
					} catch (MAPIException $e) {
						$this->processException($e, $actionType);
					}
				}
			}
		}

		/**
		 * Function will retrieve all settings stored in PR_EC_WEBACCESS_SETTINGS_JSON property
		 * if proeprty is not defined then it will return generate SettingsException but silently ignores it.
		 *
		 * @return boolean true if no exception generated else false.
		 */
		function retrieveAll($type)
		{
			$data = $GLOBALS['settings']->get();

			$this->addActionData($type, $data);
			$GLOBALS["bus"]->addData($this->getResponseData());

			$result = true;
		}

		/**
		 * Function will set a value of a setting indicated by path of the setting.
		 * 
		 * @param $settings object/array Object containing a $path and $value of the setting
		 * which must be modified.
		 * @return boolean returns true if setting is properly saved and no exception is generated else false.
		 */
		function set($settings)
		{
			if (isset($settings)) {
				if (is_array($settings)) {
					// If multiple settings are provided, we will set the settings
					// but wait with saving until the entire batch has been applied.
					foreach ($settings as $setting) {
						if (isset($setting['path']) && isset($setting['value'])) {
							$GLOBALS['settings']->set($setting['path'], $setting['value']);
						}
					}
				} else if (isset($settings['path']) && isset($settings['value'])) {
						$GLOBALS['settings']->set($settings['path'], $settings['value']);
				}

				// send success notification to client
				$this->sendFeedback(true);

				$result = true;
			}
		}

		/**
		 * Function will delete a setting indicated by setting path.
		 * 
		 * @param $path string/array path of the setting that needs to be deleted
		 * @return boolean returns true if setting is properly deleted and no exception is generated else false.
		 */
		function deleteSetting($path)
		{
			if(isset($path)) {
				if (is_array($path)) {
					// If multiple settings are provided, we will set the settings
					// but wait with saving until the entire batch has been applied.
					foreach ($path as $item) {
						$GLOBALS['settings']->delete($item);
					}
				} else {
					$GLOBALS['settings']->delete($path);
				}

				// send success notification to client
				$this->sendFeedback(true);
			}
		}
	}
?>
