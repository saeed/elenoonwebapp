<?php
	/**
	 * StickyNote ItemModule
	 * Module which openes, creates, saves and deletes an item. It 
	 * extends the Module class.
	 */
	class StickyNoteItemModule extends ItemModule
	{
		var $plaintext;

		/**
		 * Constructor
		 * @param int $id unique id.
		 * @param array $data list of all actions.
		 */
		function StickyNoteItemModule($id, $data)
		{
			$this->properties = $GLOBALS["properties"]->getStickyNoteProperties();
			
			$this->plaintext = true;
			
			parent::ItemModule($id, $data);
		}
	}
?>
